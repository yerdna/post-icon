/** Javascript for the plugin settings page.
 *
 * @file
 *
 * @package Post_Icon
 */

jQuery(
	function ($) {
		let dashicon_is_checked = function(elem) {
			if ($( elem ).prop( "checked" )) {
				let parent = $( elem ).parent();
				let icon   = /dashicons-([a-z0-9-]+)/.exec( $( parent ).attr( 'class' ) );
				$( elem ).parent().addClass( "icon-is-checked" );
				$( "#dashicon-checked" ).text( icon[1] );
			}
		}
		$( "label.dashicons input[type='radio']" ).each(
			function (index, element) {
				dashicon_is_checked( element );
			}
		);
		$( "label.dashicons input[type='radio']" ).click(
			function (event) {
				$( "label.dashicons" ).removeClass( "icon-is-checked" );
				dashicon_is_checked( event.target );
			}
		);

		let ajax_search_url = ajaxurl + '?action=post_icon_search_posts';
		$( "#search_posts" ).on(
			'change keyup',
			function(e){
				if (e.keyCode == 13) {
					$( '#post_search_results' ).html( '' );
				}
				let s = $( "#search_posts" ).val().trim();
				if (s.length > 2) {
					$.post(
						{
							"url": ajaxurl,
							"data": {
								"action": "post_icon_search_posts",
								"s": s
							},
							"success": function(response){
								$( '#post_search_results' ).html( '' );
								if (response.posts) {
									let html = '';
									response.posts.forEach(
										function(item) {
											let title = `${item.post_title} (${item.post_type} ${item.post_status})`;
											html     += `<a href="#" data-post-id="${item.ID}">${title}</a><br> `;
										}
									);
									$( '#post_search_results' ).html( html );
									$( '#post_search_results a' ).click(
										function(e){
											let id    = $( e.target ).data( 'post-id' );
											let text  = $( e.target ).text();
											let input = `<input type="checkbox" name="post_icon[posts][]" value="${id}" checked>${text}<br> `;
											$( "#posts-with-icon" ).prepend( input );
											return false;
										}
									);

								}
							}
						}
					);
				}
			}
		);
	}
);

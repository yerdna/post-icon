<?php
/** Describes main class of the Post Icon plugin.
 *
 * @file
 *
 * @package Post_Icon
 */

/**
 * Class Post_Icon
 * Main plugin class
 */
class Post_Icon {

	const OPTION_NAME = 'post_icon';
	/**
	 * Plugin settings
	 *
	 * @var mixed|void
	 */
	private $settings;
	/**
	 * Plugin directory path relatively to plugins directory
	 *
	 * @var string
	 */
	private $rel_path;

	/**
	 * Construct
	 */
	public function __construct() {
		$this->settings = get_option(
			self::OPTION_NAME,
			array(
				'posts'       => array(),
				'position'    => '',
				'icon'        => '',
				'fix_escaped' => '',
			)
		);
		$this->rel_path = basename( dirname( __DIR__ ) );
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_filter( 'the_title', array( $this, 'the_title' ), 10, 2 );
		add_action( 'wp_ajax_post_icon_search_posts', array( $this, 'ajax_search_posts' ) );
	}

	/**
	 * WP action 'init'
	 */
	public function init() {
		load_plugin_textdomain( 'post-icon', false, $this->rel_path . '/languages' );
	}

	/**
	 * WP action 'admin_init'
	 */
	public function admin_init() {
		$page = self::OPTION_NAME;
		add_settings_section( 'default', '', '__return_empty_string', $page );
		add_settings_field(
			'post-icon-posts',
			__( 'Posts', 'post-icon' ),
			array( $this, 'setting_posts' ),
			$page,
			'default'
		);
		add_settings_field(
			'post-icon-position',
			__( 'Icon Position', 'post-icon' ),
			array( $this, 'setting_position' ),
			$page,
			'default'
		);
		add_settings_field(
			'post-icon-icon',
			__( 'Icon', 'post-icon' ),
			array( $this, 'setting_icons' ),
			self::OPTION_NAME,
			'default'
		);
		add_settings_field(
			'post-icon-fix-escaped',
			__( 'Fix escaped titles', 'post-icon' ),
			array( $this, 'setting_fix_escaped' ),
			$page,
			'default'
		);

		$args = array(
			'type'              => 'array',
			'sanitize_callback' => array( $this, 'sanitize_callback' ),
		);
		register_setting( self::OPTION_NAME . '_group', self::OPTION_NAME, $args );
	}

	/**
	 * WP action 'wp_enqueue_scripts'
	 */
	public function wp_enqueue_scripts() {
		if ( ! empty( $this->settings['icon'] ) ) {
			wp_enqueue_style( 'dashicons' );
		}
	}

	/**
	 * WP action 'admin_menu'
	 */
	public function admin_menu() {
		$menu_slug = self::OPTION_NAME;
		add_options_page(
			'Post Icon',
			'Post Icon',
			'manage_options',
			$menu_slug,
			array( $this, 'settings_page' )
		);
	}

	/**
	 * WP filter 'the_title'
	 *
	 * @param string $title Post title.
	 * @param int    $post_id Post ID.
	 *
	 * @return string
	 */
	public function the_title( $title, $post_id = 0 ) {
		global $post;
		if ( empty( $post_id ) && ! empty( $post->ID ) ) {
			$post_id = $post->ID;
		}
		if ( ! is_admin() && is_main_query() ) {
			if ( ! empty( $this->settings['posts'] ) && in_array( $post_id, $this->settings['posts'] ) ) {
				if ( ! empty( $this->settings['icon'] ) ) {
					if ( ! empty( $this->settings['position'] ) ) {
						$icon  = '<span class="dashicons dashicons-' . $this->settings['icon'] . '"></span>';
						$title = 'left' === $this->settings['position'] ? $icon . ' ' . $title : $title . ' ' . $icon;
					}
				}
			}
		}
		return $title;
	}

	/**
	 * WP action 'admin_enqueue_scripts'
	 */
	public function admin_enqueue_scripts() {
		$plugin_dir_url = apply_filters( 'post_icon_plugin_dir_url', plugins_url( $this->rel_path . '/' ) );
		wp_enqueue_style( 'post-icon-settings', $plugin_dir_url . 'assets/admin.css', array(), '0.1.0' );
		wp_enqueue_script( 'post-icon-settings', $plugin_dir_url . 'assets/admin.js', array( 'jquery' ), '0.1.0', false );
	}

	/**
	 * Create plugin settings page.
	 * See admin_menu().
	 */
	public function settings_page() {
		if ( ! empty( $_GET['noheader'] ) ) {
			deactivate_plugins( 'post-icon/post-icon.php' );
			wp_redirect( admin_url( 'plugins.php' ) );
			die;
		}
		?>
		<div class="wrap">
			<h2>Post Icon</h2>
			<form method="post" action="options.php">
				<?php
				settings_fields( self::OPTION_NAME . '_group' );
				do_settings_sections( self::OPTION_NAME );
				?>
				<?php submit_button(); ?>
				<p>
					<a class="button button-secondary"
						href="<?php echo admin_url( 'options-general.php?page=post_icon&noheader=1' ); ?>">
						<?php esc_html_e( 'Deactivate', 'post-icon' ); ?>
					</a>
				</p>
			</form>
		</div>
		<?php
	}

	/**
	 * Display position setting field.
	 */
	public function setting_position() {
		$position      = $this->settings['position'];
		$checked_left  = 'left' === $position ? 'checked' : '';
		$checked_right = 'right' === $position ? 'checked' : '';
		?>
		<p>
			<input type="radio" name="post_icon[position]"
				value="left" <?php echo $checked_left; ?>> <?php esc_html_e( 'Left', 'post-icon' ); ?>
			<input type="radio" name="post_icon[position]"
				value="right" <?php echo $checked_right; ?>> <?php esc_html_e( 'Right', 'post-icon' ); ?>
		</p>
		<?php
	}

	/**
	 * Display icon setting area & field.
	 */
	public function setting_icons() {
		$icon = $this->settings['icon'];
		?>
		<div id="icon-list">
			<?php
			$css = file_get_contents( ABSPATH . 'wp-includes/css/dashicons.css' );
			preg_match_all( '/\.dashicons-([^:]+):before/um', $css, $match, PREG_PATTERN_ORDER );
			$icons = $match[1];
			unset( $icons[0] );
			foreach ( $icons as $item ) :
				echo '<span></span><label class="dashicons dashicons-' . $item . '">';
				echo '<input ' . ( $icon === $item ? 'checked ' : '' ) . ' name="post_icon[icon]" type="radio" value="' . $item . '">';
				echo '</label></span> ';
			endforeach;
			?>
			<p><?php esc_html_e( 'Checked', 'post-icon' ); ?>: <span id="dashicon-checked"></span></p>
		</div>
		<?php
	}

	/**
	 * Display posts list with checkboxes and post search field.
	 */
	public function setting_posts() {
		$post_ids = (array) $this->settings['posts'];
		$post_ids = $post_ids ? $post_ids : array( 0 );
		$args     = array(
			'post_type'   => get_post_types(),
			'post_status' => 'any',
			'numberposts' => - 1,
			'post__in'    => $post_ids,
		);
		$posts    = get_posts( $args );
		?>
		<div>
			<div id="posts-with-icon">
				<?php
				foreach ( $posts as $post ) :
					?>
					<input type="checkbox" checked value="<?php echo $post->ID; ?>" name="post_icon[posts][]">
					<?php echo esc_html( "{$post->post_title} ({$post->post_status} {$post->post_type})" ); ?>
					<a href="<?php echo get_permalink( $post->ID ); ?>">#<?php echo $post->ID; ?></a>
					<br>
				<?php endforeach; ?>
			</div>
			<p><?php esc_html_e( 'Search posts', 'post-icon' ); ?>
				<input type="text" id="search_posts">
			</p>
			<div id="post_search_results"></div>
		</div>
		<?php
	}

	/**
	 * Display fix_escaped setting field.
	 */
	public function setting_fix_escaped() {
		$checked = $this->settings['fix_escaped'] ? ' checked' : '';
		echo '<input type="checkbox" value="1" name="post_icon[fix_escaped]"' . $checked . '> ';
		echo '<span class="description">' . esc_html( __( 'Enable icons also for WooCommerce single product page title.', 'post-icon' ) ) . '</span>';
	}

	/**
	 * Sanitize settings before saving to DB, see admin_init().
	 *
	 * @param array $input Input data.
	 *
	 * @return array
	 */
	public function sanitize_callback( $input ) {
		$out['position']    = empty( $input['position'] ) ? '' : sanitize_text_field( $input['position'] );
		$out['icon']        = empty( $input['icon'] ) ? '' : sanitize_text_field( $input['icon'] );
		$out['posts']       = empty( $input['posts'] ) ? array() : array_filter( $input['posts'], 'ctype_digit' );
		$out['fix_escaped'] = empty( $input['fix_escaped'] ) ? 0 : 1;
		return $out;
	}

	/**
	 * WP action 'wp_ajax_post_icon_search_posts'
	 */
	public function ajax_search_posts() {
		if ( current_user_can( 'manage_options' ) && ! empty( $_REQUEST['s'] ) ) {
			$search   = sanitize_text_field( $_REQUEST['s'] );
			$statuses = get_post_stati();
			$types    = get_post_types( array( 'public' => true ) );
			$args     = array(
				's'           => $search,
				'numberposts' => 10,
				'post_type'   => $types,
				'post_status' => $statuses,
			);
			$posts    = get_posts( $args );
			$out      = array( 'posts' => array() );
			foreach ( $posts as $post ) {
				$out['posts'][] = array(
					'ID'          => $post->ID,
					'post_type'   => $post->post_type,
					'post_title'  => $post->post_title,
					'post_status' => $post->post_status,
				);
			}
			wp_send_json( $out );
		}
	}
}

new Post_Icon();

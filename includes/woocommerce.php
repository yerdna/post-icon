<?php
/** Fixes for WooCommerce.
 *
 * @file
 *
 * @package Post_Icon
 */

$option = get_option( 'post_icon' );
if ( ! empty( $option['fix_escaped'] ) ) {
	if ( ! function_exists( 'woocommerce_template_single_title' ) ) {
		/**
		 * Replace WooCommerce woocommerce_template_single_title function.
		 */
		function woocommerce_template_single_title() {
			?><h1 class="product_title entry-title">
				<?php the_title(); ?>
			</h1>
			<?php
		}
	}
}

��          |       �       �   2   �        
     <   #     `     s     x     �     �     �     �  �  �  Y   �     �     �  �     6   �     �     �     �  
   �     
        Add an icon (from Dashicons set) near post titles. Checked Deactivate Enable icons also for WooCommerce single product page title. Fix escaped titles Icon Icon Position Left Posts Right Search posts Project-Id-Version: Post Icon
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-09-20 14:56+0000
PO-Revision-Date: 2020-09-20 14:56+0000
Last-Translator: Andrew Kovba
Language-Team: Українська
Language: uk
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.4.3; wp-5.5 Додає іконку (з набору Dashicons) до заголовку постів. Обрано Деактивувати Дозволити іконки також для заголовку сторінки окремого продукту WooCommerce. Виправити захищені заголовки Іконка Розташування Ліворуч Пости Праворуч Шукати пости 
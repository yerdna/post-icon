<?php
/** Main Post Icon plugin file.
 *
 * @file
 *
 * @package Post_Icon
 */

/**
 * Plugin Name:     Post Icon
 * Plugin URI:      https://bitbucket.org/yerdna/post-icon
 * Description:     Add an icon (from Dashicons set) near post titles.
 * Author:          Andrey K.
 * Author URI:      https://profiles.wordpress.org/andreyk/
 * Text Domain:     post-icon
 * Domain Path:     /languages
 * Version:         0.1.0
 */

require 'includes/class-post-icon.php';
require 'includes/woocommerce.php';

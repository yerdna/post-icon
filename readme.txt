=== Post_Icon ===
Contributors: andreyk
Tags: posts, titles, icons
Requires at least: 5.5
Tested up to: 5.5.1
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add an icon (from Dashicons set) near post titles.

== Description ==

Adds Dashicons icon before or after post titles for selected posts.

== Installation ==

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to the plugin settings page: WP Admin - Settings - Post Icon.

== Changelog ==

= 0.1.0 =
* Initial version (2020-09-20).

